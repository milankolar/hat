__author__ = 'Milan'

import hou
import bisect

sel = hou.selectedNodes()
parms = sel[0].parms()
curFrame = hou.frame()

list = [1, 13, 30, 55]

i = bisect.bisect_left(list, curFrame)

bias = 0.3


for parm in parms:
    keyframes = parm.keyframes()
    if keyframes:
        keylist = []
        for keyframe in keyframes:
            if not keyframe.frame() == curFrame:
                keylist.append(keyframe.frame())
        #     # print str(parm) + 'frame: ' + str(keyframe.frame())
        i = bisect.bisect_left(keylist, curFrame)

        try:
        # if len(keylist) > i:
            for keyframe in keyframes:
                if keyframe.frame() == keylist[i-1]:
                    keyP = keyframe
                elif keyframe.frame() == keylist[i]:
                    keyN = keyframe

            keyDiff = keyN.value() - keyP.value()
            keyBlend = keyDiff * bias
            valueToSet = keyP.value() + keyBlend
            parm.set(valueToSet)
        except:
            hou.ui.setStatusMessage('You need to be between 2 keyframes', severity=hou.severityType.ImportantMessage)