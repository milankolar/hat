__author__ = 'Milan'

import hou
import pyside_houdini
import bisect
from PySide.QtCore import *
from PySide.QtGui import *
from PySide.QtUiTools import *


app = QApplication.instance()
if app is None:
    app = QApplication(['houdini'])

workDir = 'D:/CODE/HAT/'


def getKeyableParams(exludeParms, selected, keyableParms, parmDic, parmLabel=None):
    for node in selected:
        parmsTemp = node.parmsInFolder(['Transform'])
        parmDic[node] = []
        for parm in parmsTemp:
            if parmLabel == None:
                if parm.description() not in exludeParms:
                    # print str(parm.parmTemplate().type())
                    if str(parm.parmTemplate().type()) == 'parmTemplateType.Float':
                        parmDic[node].append(parm)
            else:
                if parm.description() == parmLabel:
                    parmDic[node].append(parm)


def getKeyedParms(selected):
    for node in selected:
        pass

def zeroAll(parmDic):
    with hou.undos.group('Zero All Controlers'):
        for node in parmDic:
            for parm in parmDic[node]:
                node.parm(parm.name()).set(0)


def keyframeAll(parmDic):
    with hou.undos.group('Key All Controlers'):
        key = hou.Keyframe()
        key.setFrame(hou.frame())

        for node in parmDic:
            for parm in parmDic[node]:
                key.setValue(parm.eval())
                node.parm(parm.name()).setKeyframe(key)

def setTweenPose(bias):
    with hou.undos.group('Tween'):
        selected = hou.selectedNodes()
        parmDic = {}
        exludeParms = ()
        keyableParms = []

        getKeyableParams(exludeParms, selected, keyableParms, parmDic)

        print parmDic

        curFrame = hou.frame()
        for node in parmDic:
            for parm in parmDic[node]:
                keyframes = parm.keyframes()
                if keyframes:
                    keylist = []
                    for keyframe in keyframes:
                        if not keyframe.frame() == curFrame:
                            keylist.append(keyframe.frame())
                    i = bisect.bisect_left(keylist, curFrame)
                    try:
                        for keyframe in keyframes:
                            if keyframe.frame() == keylist[i-1]:
                                keyP = keyframe
                            elif keyframe.frame() == keylist[i]:
                                keyN = keyframe
                        keyDiff = keyN.value() - keyP.value()
                        keyBlend = keyDiff * bias
                        valueToSet = keyP.value() + keyBlend
                        if i != 0:
                            parm.set(valueToSet)
                        else:
                            raise
                    except:
                        hou.ui.setStatusMessage('You need to be between 2 keyframes', severity=hou.severityType.ImportantMessage)



class HAT_main(QWidget):
    def __init__(self, parent=None):
        super(HAT_main, self).__init__(parent)

        # ui loader
        self.loader = QUiLoader()
        self.ui = self.loader.load(workDir + 'Resources/HAT_MainWindow.ui')

        # ############ GET BUTTONS
        self.keyAll = self.ui.findChild(QPushButton, 'KeyAll_btn')
        self.zeroAll = self.ui.findChild(QPushButton, 'ZeroAll_btn')
        self.translate = self.ui.findChild(QPushButton, 'translate_btn')
        self.rotate = self.ui.findChild(QPushButton, 'rotate_btn')
        self.scale = self.ui.findChild(QPushButton, 'scale_btn')
        self.tween_button = self.ui.findChild(QPushButton, 'tween_button')
        self.tweenPlus_button = self.ui.findChild(QPushButton, 'tweenPlus_button')
        self.tweenMinus_button = self.ui.findChild(QPushButton, 'tweenMinus_button')
        self.tween_slider = self.ui.findChild(QSlider, 'tween_slider')


        value = .5
        # ############ CONNECT BUTTONS
        self.keyAll.clicked.connect(self.keysButtons)
        self.zeroAll.clicked.connect(self.keysButtons)
        self.translate.clicked.connect(self.keysButtons)
        self.rotate.clicked.connect(self.keysButtons)
        self.scale.clicked.connect(self.keysButtons)
        self.tween_button.clicked.connect(self.setTweenPoseButton)
        self.tweenPlus_button.clicked.connect(self.setTweenPoseButton)
        self.tweenMinus_button.clicked.connect(self.setTweenPoseButton)
        self.tween_slider.valueChanged[int].connect(self.setTweenPoseSlider)
        self.tween_slider.sliderPressed.connect(self.setTweenPoseSlider)


        self.ui.setWindowFlags(Qt.WindowStaysOnTopHint)
        self.ui.show()


    def keysButtons(self):
        selected = hou.selectedNodes()
        parmDic = {}
        exludeParms = ('Scale', 'Uniform Scale')
        keyableParms = []

        sendBtn = self.sender().text()

        if sendBtn == 'Channel Editor':
            channelEditor = hou.ui.curDesktop().createFloatingPanel(hou.paneTabType.ChannelEditor)

        elif sendBtn == 'Select All':
            allNodes = selected[0].allSubChildren(True)

            with hou.undos.group('Select All Controlers'):
                for node in allNodes:
                    node.setSelected(False, False)
                    if node.color().rgb()[1] > 0.26 and node.color().rgb()[1] < 0.533:
                        node.setSelected(True, False, True)

        elif sendBtn == 'Key All':
            getKeyableParams(exludeParms, selected, keyableParms, parmDic)
            keyframeAll(parmDic)
        elif sendBtn == 'Zero All':
            getKeyableParams(exludeParms, selected, keyableParms, parmDic)
            zeroAll(parmDic)
        elif sendBtn in ('Translate', 'Rotate', 'Scale'):
            getKeyableParams(exludeParms, selected, keyableParms, parmDic, parmLabel=sendBtn)
            keyframeAll(parmDic)

    def setTweenPoseSlider(self):
        sldrSnd = self.sender()
        valueFloat = sldrSnd.value() * 0.01
        setTweenPose(valueFloat)

    def setTweenPoseButton(self):
        value = self.sender().statusTip()
        setTweenPose(float(value))
        self.tween_slider.setValue(50)



# dialog = HAT_main()
# dialog.show()

def runApp():
    dialog = HAT_main()
    pyside_houdini.exec_(app, dialog)